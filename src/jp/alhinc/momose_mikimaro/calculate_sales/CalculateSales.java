package jp.alhinc.momose_mikimaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args){
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> shopMap = new HashMap<String, String>();
		Map<String, Long> payOffMap = new HashMap<String, Long>();
		long sum = 0;

		//ファイル入力メソッドの呼び出し
		if(!(shopFileRead(args[0], "branch.lst", shopMap, payOffMap))) {
			return;
		}

		//連番チェック
		File dir = new File(args[0]);
		File[] list = dir.listFiles();
		List<String> rcdNumber = new ArrayList<String>();
		List<String> rcdFull = new ArrayList<String>();
		BufferedReader br = null;

		//ファイルの名前を取得
        for(int i = 0; i < list.length; i++){
        	String payOff = list[i].getName();

        	//条件に合うものを取得
        	if(list[i].isFile() && payOff.matches("[0-9]{8}\\.rcd")){	//←ファイルかつ数字8桁の末尾rcdのファイルを抽出
        		String[] rcdFile = payOff.split("\\.");
        		rcdNumber.add(rcdFile[0]);		//rcdNumerに桁数を格納
        		rcdFull.add(payOff);

        	}
        }

        Collections.sort(rcdNumber);
        for(int i = 0; i < rcdNumber.size() - 1; i++) {
        	int rcd = Integer.parseInt(rcdNumber.get(i));
        	int rcd2 = Integer.parseInt(rcdNumber.get(i + 1));
        	if(rcd2 - rcd != 1) {
        		System.out.println("売上ファイル名が連番になっていません");
        		return;
        	}
        }
        //売上集計
        for(int t = 0; t < rcdFull.size(); t++) {
        	List<String> array = new ArrayList<String>();


            try{

    			File file = new File(args[0], rcdFull.get(t));
    			FileReader fr = new FileReader(file);
    			br = new BufferedReader(fr);

    			String line;


				while((line = br.readLine()) != null){

    				array.add(line);
    			}
				sum = payOffMap.get(array.get(0)) + Long.parseLong(array.get(1)); //計算前の集計(0) と売上の足し算 Long.parseLongで型変換)
    			payOffMap.put(array.get(0), sum);

    			if(!array.get(0).matches("[0-9]{3}") ) {
    				System.out.println(rcdFull.get(t) + "の支店コードが不正です");		//支店コードの確認コード
    				return;
    			}
    			//売上に数字以外が入っていないかチェック
    			if(array.get(1).matches("[^0-9]")) {
    				System.out.println("予期せぬエラーが発生しました");
    				return;
    			}

    			if(array.get(0) == null || array.size() > 2) {
    				System.out.println(rcdFull.get(t) + "のフォーマットが不正です");  //売上集計ファイルの中身が1桁、もしくは3桁以上ある場合
    				return;
    			}
    			int digit = String.valueOf(sum).length();
    			if(digit > 10) {
    				System.out.println("合計金額が10桁を超えました");
    				return;
    			}
           }catch(IOException e) {
    			System.out.println("予期せぬエラーが発生しました。");
    			return;
    		}finally{
    			if(br != null){
    				try{
    					br.close();
    				}catch(IOException e){
    					System.out.println("予期せぬエラーが発生しました");
    					return;
    				}
    			}
    		}

        }
        //ファイル出力メソッドの呼び出し
        if(!salesTotalFile(args[0], "branch.out",payOffMap, shopMap)) {
        	return;
        }
   	}

	//ファイル入力メソッド
	public static boolean shopFileRead(String folderPoint, String fileName, Map<String, String> shopName, Map<String, Long> payOffMap) {
		BufferedReader br = null;
		try {
			File file = new File(folderPoint, fileName);
			if(!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String[] s = line.split(","); //splitメソッドでカンマで分割
				if((s[0].matches("![0-9]{3}+$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!(s.length == 2)){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				int digit = s[0].length();
    			if(!(digit == 3)) {
    				System.out.println("支店定義ファイルのフォーマットが不正です");
    				return false;
    			}

				shopName.put(s[0], s[1]);  //	コードと支店名を紐付ける
				payOffMap.put(s[0], 0L);		//共通コードに売り上げ0円を紐付け
			}

		}catch(IOException e1) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		catch(NullPointerException e2) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}finally {
				if(br != null){
					try{
						br.close();
					}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
					}
				}

		}
		return true;
	}

	//ファイル出力メソッド
	public static boolean salesTotalFile(String folderPoint,String fileName, Map<String, Long> payOffMap, Map<String, String> shopName) {
        BufferedWriter bw = null;
        try{

	        File file = new File(folderPoint, fileName);
	        FileWriter fw = new FileWriter(file);
	        bw = new BufferedWriter(fw);

	        for(Map.Entry<String, Long> entry : payOffMap.entrySet()) {
	        	bw.write(entry.getKey() + "," + shopName.get(entry.getKey()) + "," + entry.getValue());
	        	bw.newLine();
	        	}
	        return true;
        }catch(IOException e){
        	System.out.println("予期せぬエラーが発生しました");
        	return false;
        }finally{
        	try{{
        	bw.close();							//BufferedWriterとBufferedReaderは原則finally内でcloseする。
        	}}catch(Exception e){
        		System.out.println("予期せぬエラーが発生しました");
        		return false;
        	}
        }
	}

}